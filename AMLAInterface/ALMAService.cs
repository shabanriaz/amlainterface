﻿using SKYServices.Services;
using System.ServiceProcess;
using System.Configuration;
using System.Net.Http;

namespace AMLAInterface
{
    public partial class ALMAService : ServiceBase
    {
        public ALMAService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {

        }

        protected override void OnStop()
        {
            this._eventLog.Dispose();
        }

    }
}
