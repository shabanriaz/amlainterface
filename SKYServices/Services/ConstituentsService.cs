﻿using System;
using System.Net.Http;

namespace SKYServices.Services
{
    public class ConstituentsService : IConstituentsService
    {

        private Uri _apiBaseUri;
        private readonly AppSettings _appSettings;
        private readonly ISessionService _sessionService;
        private readonly IAuthenticationService _authService;

        public ConstituentsService(AppSettings appSettings, ISessionService sessionService, IAuthenticationService authService)
        {
            _appSettings = appSettings;
            _sessionService = sessionService;
            _authService = authService;
            _apiBaseUri = new Uri(new Uri(_appSettings.SkyApiBaseUri), "constituent/v1/");
        }


        /// <summary>
        /// Requests that the auth service refresh the access token and returns true if successful.
        /// </summary>
        private bool TryRefreshToken()
        {
            HttpResponseMessage tokenResponse = _authService.RefreshAccessToken();
            return (tokenResponse.IsSuccessStatusCode);
        }


        /// <summary>
        /// Performs HTTP requests (POST/GET) and returns the response.
        /// <param name="method" type="String">The HTTP method, post, get</param>
        /// <param name="endpoint" type="String">The API endpoint</param>
        /// <param name="content" type="HttpContent">The request body content</param>
        /// </summary>
        private HttpResponseMessage Proxy(string method, string endpoint, StringContent content = null)
        {
            using (HttpClient client = new HttpClient())
            {
                client.Timeout = new TimeSpan(0,5,0);
                string token = _sessionService.GetAccessToken();
                HttpResponseMessage response = null;

                // Set constituent endpoint.
                client.BaseAddress = _apiBaseUri;

                // Set request headers.
                client.DefaultRequestHeaders.Add("bb-api-subscription-key", _appSettings.AuthSubscriptionKey);
                client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Bearer " + token);

                // Make the request to constituent API.
                try
                {
                    switch (method.ToLower())
                    {
                        default:
                        case "get":
                            response = client.GetAsync(endpoint).Result;
                            break;

                        case "post":
                            response = client.PostAsync(endpoint, content).Result;
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                return response;
            }
        }


        /// <summary>
        /// Returns a response containing a constituent record (from an ID).
        /// </summary>
        public HttpResponseMessage GetConstituent(string id)
        {
            // Make the request.
            HttpResponseMessage response = Proxy("get", "constituents/" + id);

            if (response == null)
            {
                return response;
            }

            // Handle bad response.
            if (!response.IsSuccessStatusCode)
            {
                int statusCode = (int)response.StatusCode;
                switch (statusCode)
                {
                    // ID formatted incorrectly.
                    case 400:
                        response.Content = new StringContent("{ error: \"The specified constituent ID was not in the correct format.\" }");
                        break;

                    // Token expired. Refresh the token and try again.
                    case 401:
                        bool tokenRefreshed = TryRefreshToken();
                        if (tokenRefreshed)
                        {
                            response = Proxy("get", "constituents/" + id);
                        }
                        break;

                    // Constituent not found.
                    case 404:
                        response.Content = new StringContent("{ error: \"No constituent record was found with the specified ID.\" }");
                        break;
                }
            }

            return response;
        }


        /// <summary>
        /// Returns a response containing a paginated list of constituents.
        /// </summary>
        public HttpResponseMessage GetConstituents(long offset)
        {
            HttpResponseMessage response;
            string endPoint = "";
            if (offset == 0)
            {
                endPoint = "constituents/?include_inactive=false&include_deceased=false";
                response = Proxy("get", endPoint);
            }
            else
            {
                endPoint = "constituents/?include_inactive=false&include_deceased=false&offset=" + offset.ToString();
                response = Proxy("get", endPoint);
            }

            if (response == null)
            {
                return response;
            }

            // Handle bad response.
            if (!response.IsSuccessStatusCode)
            {
                int statusCode = (int)response.StatusCode;
                switch (statusCode)
                {                  
                    // Token expired. Refresh the token and try again.
                    case 401:
                        bool tokenRefreshed = TryRefreshToken();
                        if (tokenRefreshed)
                        {
                            response = Proxy("get", endPoint);
                        }
                        break;

                    // Constituent not found.
                    case 404:
                        response.Content = new StringContent("{ error: \"No constituent record was found\" }");
                        break;
                }
            }
            return response;
        }
    }
}
