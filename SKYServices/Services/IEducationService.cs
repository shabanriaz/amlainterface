﻿using System.Net.Http;
namespace SKYServices.Services
{
    public interface IEducationService
    {
        HttpResponseMessage GetConstituentEducation(string id);
        HttpResponseMessage GetEducations(long offset);
    }
}
