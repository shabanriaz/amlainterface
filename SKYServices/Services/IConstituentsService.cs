﻿using System.Net.Http;

namespace SKYServices.Services
{
    public interface IConstituentsService
    {
        HttpResponseMessage GetConstituent(string id);
        HttpResponseMessage GetConstituents(long offset);
    }
}
