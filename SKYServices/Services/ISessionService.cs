﻿using System.Net.Http;

namespace SKYServices.Services
{
    public interface ISessionService
    {
        void SetTokens(HttpResponseMessage response);
        void ClearTokens();
        string GetAccessToken();
        string GetRefreshToken();
        void UpdateRefreshToken(string refreshToken);
    }
}
