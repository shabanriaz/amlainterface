﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace SKYServices.Services
{
    public class SessionService : ISessionService
    {        
        private string _accessToken;
        private string _refreshToken;

        public SessionService(string accessToken, string refreshToken)
        {
            _accessToken = accessToken;
            _refreshToken = refreshToken;
        }


        /// <summary>
        /// Destroys access and refresh tokens.
        /// </summary>
        public void ClearTokens()
        {
            _accessToken = "";
            _refreshToken = "";
        }


        /// <summary>
        /// Return access token, if saved, or an empty string.
        /// </summary>
        public string GetAccessToken()
        {
            return _accessToken;
        }


        /// <summary>
        /// Return refresh token, if saved, or an empty string.
        /// </summary>
        public string GetRefreshToken()
        {
            return _refreshToken;
        }


        /// <summary>
        /// Sets the access and refresh tokens based on an HTTP response.
        /// </summary>
        public void SetTokens(HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode)
            {
                string jsonString = response.Content.ReadAsStringAsync().Result;
                Dictionary<string, string> attrs = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonString);
                _accessToken = attrs["access_token"];
                _refreshToken = attrs["refresh_token"];

                UpdateRefreshToken(_refreshToken);
            }
        }

        /// <summary>
        /// Save the updated refresh token to settings.json file in bin folder.
        /// </summary>
        public void UpdateRefreshToken(string refreshToken)
        {
            JObject jObject = new JObject();
            jObject["settings"] = new JObject(new JProperty("RefreshToken", refreshToken));
            File.WriteAllText(@"settings.json", jObject.ToString());
        }
    }
}
