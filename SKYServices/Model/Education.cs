﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKYServices.Model
{
    public partial class Education
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("class_of")]
        public string ClassOf { get; set; }

        [JsonProperty("constituent_id")]
        public string ConstituentId { get; set; }

        [JsonProperty("date_added")]
        public System.DateTime DateAdded { get; set; }

        [JsonProperty("date_entered")]
        public Date DateEntered { get; set; }

        [JsonProperty("date_graduated")]
        public Date DateGraduated { get; set; }

        [JsonProperty("date_modified")]
        public System.DateTime DateModified { get; set; }

        [JsonProperty("degree")]
        public string Degree { get; set; }

        [JsonProperty("primary")]
        public bool Primary { get; set; }

        [JsonProperty("school")]
        public string School { get; set; }

        [JsonProperty("social_organization")]
        public string SocialOrganization { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("campus")]
        public string Campus { get; set; }

        [JsonProperty("known_name")]
        public string KnownName { get; set; }

        [JsonProperty("date_left")]
        public DateLeft DateLeft { get; set; }

        [JsonProperty("minors")]
        public List<string> Minors { get; set; }
    }
}
