﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SKYServices.Model
{
    public partial class Email
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("constituent_id")]
        public string ConstituentId { get; set; }

        [JsonProperty("do_not_email")]
        public bool? DoNotEmail { get; set; }

        [JsonProperty("inactive")]
        public bool Inactive { get; set; }

        [JsonProperty("primary")]
        public bool Primary { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }

}
