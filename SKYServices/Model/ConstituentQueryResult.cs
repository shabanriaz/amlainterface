﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using SKYServices.Model;
//
//    var constituentQueryResult = ConstituentQueryResult.FromJson(jsonString);

namespace SKYServices.Model
{
    using System;
    using System.Net;
    using System.Collections.Generic;

    using Newtonsoft.Json;

    public partial class ConstituentQueryResult
    {
        [JsonProperty("count")]
        public long Count { get; set; }

        [JsonProperty("next_link")]
        public string NextLink { get; set; }

        [JsonProperty("value")]
        public List<Constituent> Constituents { get; set; }
    }

   

    public partial class Seasonal
    {
        [JsonProperty("d")]
        public long D { get; set; }

        [JsonProperty("m")]
        public long M { get; set; }
    }

    public partial class Birthdate
    {
        [JsonProperty("d")]
        public long D { get; set; }

        [JsonProperty("m")]
        public long M { get; set; }

        [JsonProperty("y")]
        public long Y { get; set; }
    }

   

    public partial class ConstituentQueryResult
    {
        public static ConstituentQueryResult FromJson(string json) => JsonConvert.DeserializeObject<ConstituentQueryResult>(json, SKYServices.Model.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this ConstituentQueryResult self) => JsonConvert.SerializeObject(self, SKYServices.Model.Converter.Settings);
        public static string ToJson(this EducationQueryResult self) => JsonConvert.SerializeObject(self, SKYServices.Model.Converter.Settings);
    }

    public class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
