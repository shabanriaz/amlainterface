﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SKYServices.Model
{
    public partial class Address
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("address_lines")]
        public string AddressLines { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("constituent_id")]
        public string ConstituentId { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("do_not_mail")]
        public bool DoNotMail { get; set; }

        [JsonProperty("formatted_address")]
        public string FormattedAddress { get; set; }

        [JsonProperty("inactive")]
        public bool Inactive { get; set; }

        [JsonProperty("postal_code")]
        public string PostalCode { get; set; }

        [JsonProperty("preferred")]
        public bool Preferred { get; set; }

        [JsonProperty("start")]
        public System.DateTime? Start { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("seasonal_end")]
        public Seasonal SeasonalEnd { get; set; }

        [JsonProperty("seasonal_start")]
        public Seasonal SeasonalStart { get; set; }

        [JsonProperty("end")]
        public System.DateTime? End { get; set; }
    }
}
