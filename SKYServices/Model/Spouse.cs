﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SKYServices.Model
{
    public partial class Spouse
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("first")]
        public string First { get; set; }

        [JsonProperty("last")]
        public string Last { get; set; }
    }
}
