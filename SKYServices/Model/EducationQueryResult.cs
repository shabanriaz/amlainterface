﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using SKYServices.Model;
//
//    var educationQueryResult = EducationQueryResult.FromJson(jsonString);

namespace SKYServices.Model
{
    using System;
    using System.Net;
    using System.Collections.Generic;

    using Newtonsoft.Json;

    public partial class EducationQueryResult
    {
        [JsonProperty("count")]
        public long Count { get; set; }

        [JsonProperty("next_link")]
        public string NextLink { get; set; }

        [JsonProperty("value")]
        public List<Education> Educations { get; set; }
    }

    

    public partial class Date
    {
        [JsonProperty("d")]
        public long D { get; set; }

        [JsonProperty("m")]
        public long M { get; set; }

        [JsonProperty("y")]
        public long Y { get; set; }
    }

    public partial class DateLeft
    {
        [JsonProperty("y")]
        public long Y { get; set; }
    }

    public partial class EducationQueryResult
    {
        public static EducationQueryResult FromJson(string json) => JsonConvert.DeserializeObject<EducationQueryResult>(json, SKYServices.Model.Converter.Settings);
    }
}
