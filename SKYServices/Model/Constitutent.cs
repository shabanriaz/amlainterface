﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace SKYServices.Model
{
    public partial class Constituent
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("address")]
        public Address Address { get; set; }

        [JsonProperty("age")]
        public long? Age { get; set; }

        [JsonProperty("birthdate")]
        public Birthdate Birthdate { get; set; }

        [JsonProperty("date_added")]
        public System.DateTime DateAdded { get; set; }

        [JsonProperty("date_modified")]
        public System.DateTime DateModified { get; set; }

        [JsonProperty("deceased")]
        public bool? Deceased { get; set; }

        [JsonProperty("email")]
        public Email Email { get; set; }

        [JsonProperty("first")]
        public string First { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("gives_anonymously")]
        public bool GivesAnonymously { get; set; }

        [JsonProperty("inactive")]
        public bool Inactive { get; set; }

        [JsonProperty("last")]
        public string Last { get; set; }

        [JsonProperty("lookup_id")]
        public string LookupId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("phone")]
        public Phone Phone { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("former_name")]
        public string FormerName { get; set; }

        [JsonProperty("online_presence")]
        public Email OnlinePresence { get; set; }

        [JsonProperty("middle")]
        public string Middle { get; set; }

        [JsonProperty("preferred_name")]
        public string PreferredName { get; set; }

        [JsonProperty("spouse")]
        public Spouse Spouse { get; set; }

        [JsonProperty("suffix")]
        public string Suffix { get; set; }

        [JsonProperty("suffix_2")]
        public string Suffix2 { get; set; }

        [JsonProperty("title_2")]
        public string Title2 { get; set; }

        [JsonProperty("marital_status")]
        public string MaritalStatus { get; set; }
    }
}
