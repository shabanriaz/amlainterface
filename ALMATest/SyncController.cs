﻿using System;
using SKYServices.Services;
using System.Configuration;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using SKYServices.Model;

namespace ALMATest
{
    public class SyncController
    {
        private AppSettings _appSettings;

        public SyncController()
        {
            // Create application settings object to pass to service request.
            _appSettings = new AppSettings();
            _appSettings.AuthBaseUri = ConfigurationManager.AppSettings["AuthBaseUri"];
            _appSettings.AuthClientId = ConfigurationManager.AppSettings["AuthClientId"];
            _appSettings.AuthClientSecret = ConfigurationManager.AppSettings["AuthClientSecret"];
            _appSettings.AuthSubscriptionKey = ConfigurationManager.AppSettings["AuthSubscriptionKey"];
            _appSettings.SkyApiBaseUri = ConfigurationManager.AppSettings["SkyApiBaseUri"];
            _appSettings.RefreshToken = ReadRefreshToken();
        }

        public void SyncData()
        {

            ISessionService sessionService;
            IAuthenticationService authenticationService;
            IConstituentsService constituentsService;
            IEducationService educationService;
            HttpResponseMessage responseMessage;
            ConstituentQueryResult constituentQueryResult = new ConstituentQueryResult();
            EducationQueryResult educationQueryResult = new EducationQueryResult();

            string jsonResult = "";
            long totalRecords= 0;
            long totalRetrieved = 0;


            // Create Session Service object to keep track of authenication token.
            sessionService = new SessionService("", _appSettings.RefreshToken);

            // Create Authentication Service object and refresh access token.
            authenticationService = new AuthenticationService(_appSettings, sessionService);
            authenticationService.RefreshAccessToken();

            // Constituent api returns data in paginated format(500 record per request), so loop to get all records.
            Console.WriteLine("-------------------- Starting Constituent Fetch --------------------");
            constituentsService = new ConstituentsService(_appSettings, sessionService, authenticationService);
            do
            {
                responseMessage = constituentsService.GetConstituents(totalRetrieved);
                if (responseMessage != null)
                {
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        jsonResult = responseMessage.Content.ReadAsStringAsync().Result;
                        // Prase Json and assign to an object.
                        if (!jsonResult.Equals(""))
                            constituentQueryResult = ConstituentQueryResult.FromJson(jsonResult);
                    }
                    if (totalRetrieved == 0)
                        totalRecords = constituentQueryResult.Count;
                    totalRetrieved += constituentQueryResult.Constituents.Count;
                    Console.WriteLine(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + " ------ Total Retrieved: " + totalRetrieved.ToString() + "-----------");
                }

            } while (totalRetrieved < totalRecords);
            Console.WriteLine("-------------------- Ending Constituent Fetch --------------------");

            // Fetch list of all educations using constituent/educations api.
            // Educations api returns data in paginated format (500 records per request), so loop to get all records.
            Console.WriteLine("-------------------- Starting Education Fetch --------------------");
            educationService = new EducationService(_appSettings, sessionService, authenticationService);
            responseMessage = null;
            totalRetrieved = 0;
            totalRecords = 0;
            do
            {
                responseMessage = educationService.GetEducations(totalRetrieved);

                if (responseMessage != null)
                {
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        jsonResult = responseMessage.Content.ReadAsStringAsync().Result;
                        // Prase Json and assign to an object.
                        if (!jsonResult.Equals(""))
                            educationQueryResult = EducationQueryResult.FromJson(jsonResult);
                    }
                    if (totalRetrieved == 0)
                        totalRecords = educationQueryResult.Count;
                    totalRetrieved += educationQueryResult.Educations.Count;
                    Console.WriteLine(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + " ------ Total Retrieved: " + totalRetrieved.ToString() + "-----------");
                }

             } while (totalRetrieved < totalRecords);
            Console.WriteLine("-------------------- Ending Education Fetch --------------------");
        }

        /// <summary>
        /// Read the refresh token from settings.json file in bin folder.
        /// </summary>
        public string ReadRefreshToken()
        {
            string existingRefreshToken = "";

            JObject jObject = JObject.Parse(File.ReadAllText(@"settings.json"));
            existingRefreshToken = (string)jObject["settings"]["RefreshToken"];

            return existingRefreshToken;
        }

    }
}
